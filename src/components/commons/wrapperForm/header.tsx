import * as React from 'react';
import {
  Header,
  Logo,
  TopMenu,
  LogIn,
  AuthButton,
  BaseButton
} from './styled';
import Identification from '../../identification';
const logo = require('./img/logo.png');

const View = (props: any) => {
  return (
        <Header>
          <Logo onClick={() => props.openApp('defaultapp.bundle.js')}>
            <img src={logo} />
          </Logo>
          <TopMenu>
            <BaseButton>Реклама на радио</BaseButton>
            <BaseButton>Реклама на транспорте</BaseButton>
            <BaseButton>Реклама в лифтах</BaseButton>
          </TopMenu>
          <LogIn>
            <Identification {...props}/>
          </LogIn>
        </Header>        
  );
};
export default View;