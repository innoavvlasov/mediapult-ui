import * as React from 'react';

import { Option, OuterList, Input, DropDownIcon, Wrapper } from './styled';

export default class Select extends React.Component<any, any> {
  constructor(props: any) {
    super(props);
    this.hide = this.hide.bind(this);
    this.toogleOuterList = this.toogleOuterList.bind(this);
    this.selected = this.selected.bind(this);
    this.state = { show: false };
  }

  generateOptions(data: any) {
    let options = data.map((item, index) => {
      return (
        <Option key={index} value={item.value}>
          <a
            className="optionSelect"
            href="#"
            onClick={() => this.selected(event, item)}
          >
            {item.label}
          </a>
        </Option>
      );
    });
    return options;
  }

  selected(e: any, value: string) {
    e.preventDefault();
    this.props.onChange(value);
    this.setState({ selectValue: value });
  }

  toogleOuterList(e: any) {
    this.setState({ show: !this.state.show });
  }

  hide(e: any) {
    if (e && e.relatedTarget && e.relatedTarget.className === 'optionSelect') {
      e.relatedTarget.click();
    }
    this.setState({ show: false });
  }

  componentWillReceiveProps(nextprops: any) {
    this.setState({ selectValue: nextprops.selected });
  }

  checkSelected(selected: any) {
    return selected.label ? selected : {label: selected, value: selected};
  }

  render() {
    let propsOptions = [];
    if (this.props.range) {
      for (var i = this.props.range.start; i <= this.props.range.end; i++) {
        propsOptions.push({ label: i, value: i });
      }
    }
    let optionsData = this.props.options ? this.props.options : propsOptions;
    let options: any = this.generateOptions(optionsData);
    let selectValue = this.state.selectValue || this.props.selected || optionsData[0];
    selectValue = this.checkSelected(selectValue);
    return (
      <Wrapper {...this.props}>
        <Input
          className={this.state.show ? 'bordered' : ''}
          type="button"
          onBlur={this.hide}
          onClick={() => this.toogleOuterList(event)}
        >
          <div className={this.props.disableDD ? 'fit' : ''}>
            {selectValue.label}
          </div>
          {!this.props.disableDD && (
            <div>
              <DropDownIcon className={this.state.show ? 'flip' : ''} />
            </div>
          )}
        </Input>
        {this.state.show && (
          <div>
            <OuterList
              className={this.state.show && options.length > 3 ? 'outer' : ''}
            >
              {options}
            </OuterList>
          </div>
        )}
      </Wrapper>
    );
  }
}
