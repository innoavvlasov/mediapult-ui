export const getCurrentDate = () => {
  let today = new Date();
  let currentDay = today.getDate();
  let currentMonth = today.getMonth();
  let currentYear = today.getFullYear();
  return {day: currentDay, month: currentMonth, year: currentYear};
};

export const getCountDay = (month: number, year: number) => {
  return new Date(year, month, 0).getDate();
};