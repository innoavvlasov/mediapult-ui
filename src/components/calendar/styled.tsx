import * as React from 'react';
import styled from 'styled-components';
import Select from '../select';

export const Wrapper: any = styled.div`
  user-select: none;
  cursor: pointer;
  position: relative;
  min-width: 170px;
  > div {
    display: inline-block;
    position: relative;
    float: left;
  }
  > div:nth-child(1) {
    width: 30px;
  }
  > div:nth-child(2) {
    margin: 0 5px;
    width: 80px;
  }
  > div:nth-child(3) {
    width: 50px;
  }
`;

export const StyledSelect: any = styled(Select)`
   > button > div {
    font-weight: 600;
   }
   .optionSelect {
    font-weight: 600;
  }
`;