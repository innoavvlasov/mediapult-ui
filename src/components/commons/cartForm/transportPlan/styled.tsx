import * as React from 'react';
import styled from 'styled-components';
import { TransportType } from './svg';

export const Wrapper = styled.div`
> div {
  display: grid;
  grid-template-columns: 370px 100px 100px 120px auto 60px;
  @media only screen and (max-width: 1200px) and (min-width: 500px) {
    grid-template-columns: 170px 100px 100px 120px auto 30px;
  }
  @media only screen and (max-width: 2500px) and (min-width: 1700px) {
    grid-template-columns: 450px 150px 150px 170px auto 110px;
  }
> div:last-child {
  border-bottom: none;
}

}
  
`;
export const Header = styled.div`
  height: 50px;
  background-color: #fafbfc;
  align-self: center;
  > div {
    color: #7f8fa4;
    font-size: 14px;
    font-family: 'ProximaNovaSemibold';
    text-align: center;
    align-self: center;
  }
  div:first-child {
    text-align: left;
    padding-left: 20px;
  }
  border-bottom: 1px solid rgb(223, 227, 233);
`;

export const TableRow = styled.div`
  height: 40px;
  padding: 20px 0;
  align-self: center;
  text-align: center;
  @media only screen and (max-width: 1200px) and (min-width: 500px) {
   height: 75px;
  }
  border-bottom: 1px solid rgb(223, 227, 233);
  > div {
    align-self: center;
  }
  &:last-child {
    border-bottom: none;
  }
`;
const ProductWrapper = styled.div`
  padding-left: 20px;
  position: relative;
  > div {
    float: left;
    display: inline-block;
    position: relative;
    @media only screen and (max-width: 1200px) and (min-width: 500px) {
    display: block;
    width: 100%
    margin: 0 auto;
  }
  }
`;
const IconBlock = styled.div`
  margin-right: 15px;

`;
const DescBlock = styled.div`
  > div {
    display: block;
    position: relative;
  }

`;
const Name = styled.div`
  color: #354052;
  font-size: 16px;
  font-family: 'ProximaNovaSemibold';
`;
const Addrees = styled.div`
  color: #7f8fa4;
  font-size: 12px;
  font-family: 'ProximaNovaRegular';
`;

export const Product = ({data: {type, name, address}}) => {
  let Icon = TransportType[type];
  return (
    <ProductWrapper>
      <IconBlock><Icon/></IconBlock>
      <DescBlock>
        <Name>{name}</Name>
        <Addrees>{address}</Addrees>
      </DescBlock>
    </ProductWrapper>
  );
};

export const Summa = styled.div`
  color: #00aeef;
  font-family: 'ProximaNovaSemibold';
  font-size: 16px;
  text-align: center;
`;

const RemoveBtnWrapper = styled.div`
  cursor: pointer;
  user-select: none;
  &:active {
    .recycle {
      fill: #ff7800;
    }
  }
`;
export const Remove = (props: any) => {
  return (
    <RemoveBtnWrapper>
     <RecycleIcon/>
    </RemoveBtnWrapper>
  );
};

const RecycleSVG = styled.svg`
  width: 16px;
  height: 16px;
`;

const RecycleIcon = (props: any) => {
    return (
    <RecycleSVG {...props} viewBox="0 0 16 16">
        <path className="recycle" fill="rgb(197, 208, 222)" d="M15.000,5.000 L15.000,15.000 C15.000,15.552 14.552,16.000 14.000,16.000 L2.000,16.000 C1.448,16.000 1.000,15.552 1.000,15.000 L1.000,5.000 L-0.000,5.000 L-0.000,4.000 C-0.000,3.448 0.448,3.000 1.000,3.000 L2.000,3.000 L3.000,3.000 L5.000,3.000 L5.000,1.000 C5.000,0.448 5.448,-0.000 6.000,-0.000 L10.000,-0.000 C10.552,-0.000 11.000,0.448 11.000,1.000 L11.000,3.000 L13.000,3.000 L14.000,3.000 L15.000,3.000 C15.552,3.000 16.000,3.448 16.000,4.000 L16.000,5.000 L15.000,5.000 ZM9.000,2.000 L7.000,2.000 L7.000,3.000 L9.000,3.000 L9.000,2.000 ZM13.000,5.000 L3.000,5.000 L3.000,14.000 L13.000,14.000 L13.000,5.000 ZM7.000,12.000 L5.000,12.000 L5.000,7.000 L7.000,7.000 L7.000,12.000 ZM11.000,12.000 L9.000,12.000 L9.000,7.000 L11.000,7.000 L11.000,12.000 Z"/>
    </RecycleSVG>
    );
};
