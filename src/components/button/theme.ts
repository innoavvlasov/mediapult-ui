import { Colors, FONTS_STACK } from '../../constants';

interface ButtonStyledProps {
  type?: ButtonType;
  disabled?: boolean;
  leftIcon?: {};
  rightIcon?: {};
}

enum ButtonType {
  Orange = 1,
  Border,
  OrangeBorder,
  White,
  Green,
  Ascetic
}

interface ButtonTheme {
  disabled: boolean;
  color: string;
  paddingLeft: string;
  paddingRight: string;
  borderColor?: string;
  backgroundColor?: string;
  verticalGradient?: VerticalGradient;
  fontsStack: string;
}

interface VerticalGradient {
  topColor: string;
  bottomColor: string;
}

const OFFSET_WITHOUT_ICON: string = '14px';
const OFFSET_WITH_ICON: string = '4px';

const getTheme: (props: ButtonStyledProps) => ButtonTheme = props => {
  const type = props.type || ButtonType.Ascetic;
  const themeAdjustments = {
    [ButtonType.Ascetic]: {
      color: Colors.white
    },
    [ButtonType.Orange]: {
      color: Colors.white,
      backgroundColor: Colors.orangeColor
    },
    [ButtonType.Border]: {
      borderColor: Colors.mainColor
    },
    [ButtonType.OrangeBorder]: {
      color: Colors.orangeColor,
      borderColor: Colors.orangeColor
    },
    [ButtonType.White]: {
      color: Colors.gray2,
      borderColor: Colors.notActiveColor,
      verticalGradient: {
        topColor: Colors.gradients.white.color1,
        bottomColor: Colors.gradients.white.color2
      }
    },
    [ButtonType.Green]: {
      color: Colors.white,
      borderColor: Colors.green,
      verticalGradient: {
        topColor: Colors.gradients.green.color1,
        bottomColor: Colors.gradients.green.color2
      }
    }
  };

  return {
    color: props.disabled ? Colors.notActiveColor : Colors.mainColor,
    borderColor: props.disabled && type !== ButtonType.Ascetic ? Colors.notActiveColor : undefined,
    paddingLeft: props.leftIcon ? OFFSET_WITH_ICON : OFFSET_WITHOUT_ICON,
    paddingRight: props.rightIcon ? OFFSET_WITH_ICON : OFFSET_WITHOUT_ICON,
    disabled: props.disabled || false,
    fontsStack: FONTS_STACK.PROXIMA_NOVA,
    ...(!props.disabled && themeAdjustments[type])
  };
};

export { ButtonStyledProps, ButtonType, ButtonTheme, getTheme };
