import * as React from 'react';
import { StyledDiv, StyledImg } from './styled';

const loaderImg = require('./img/loader.gif');

export const BaseLoader = (props: any) => (<StyledDiv><StyledImg src={loaderImg}/></StyledDiv>);