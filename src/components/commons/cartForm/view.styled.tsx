import styled from 'styled-components';

export const Wrapper = styled.div`
  display: grid;
  grid-column-gap: 30px;
  grid-template-columns: auto 270px;
  justify-content: start;  
  @media only screen and (max-width: 3840px) and (min-width: 1600px) {
    grid-template-columns: 1100px 270px;
  }
`;

export const LeftPane = styled.div`
  display: grid;
  grid-template-rows: 75px auto;
  background-color: white;
  border: 1px solid rgb(223, 227, 233);
  border-radius: 4px;
`;

export const BuyMenu = styled.div`
  border-bottom: 1px solid rgb(223, 227, 233);
  display: inline-block;
  position: relative;
  height: 74px;
  line-height: 74px;
  vertical-align: middle;
  user-select: none;
  > div {
    float: left;
    display: inline-block;
    position: relative;
    height: 72px;
    line-height: 72px;
    vertical-align: middle;
  }
`;

export const MenuItem: any = styled.div`
  float: left;
  cursor: pointer;
  display: inline-block;
  position: relative;
  height: 72px;
  line-height: 72px;
  vertical-align: middle;
  padding: 0 30px;
  ${(props: any) => {
    return props.selected && 'border-bottom: 3px solid #289cf4;';
  }};
`;
export const Title = styled.div`
  font-size: 24px;
  color: #354052;
  float: left;
  position: relative;
  height: 72px;
  line-height: 72px;
  vertical-align: middle;
  @media only screen and (max-width: 1200px) and (min-width: 500px) {
    font-size: 20px;
  }
`;
export const CountItem = styled.div`
  font-size: 14px;
  color: #ccd3da;
  float: left;
  margin-left: 10px;
  height: 72px;
  line-height: 72px;
  vertical-align: middle;
`;
export const Delimiter = styled.div`
  width: 1px;
  background-color: #ccd3da;
  margin-top: 20px;
  float: left;
  height: 30px;
  line-height: 72px;
  vertical-align: middle;
`;

export const TableBuy = styled.div``;

export const EmptyTableBuy = styled.div`
  padding: 25px;
  color: #7f8fa4;
`;

export const RightPane = styled.div`
  display: grid;
  grid-template-rows: 125px auto;
`;

export const CompleteInfo = styled.div`
  height: 300px;
  background-color: white;
  border: 1px solid rgb(223, 227, 233);
  border-radius: 4px;
`;

export const Links = styled.div`
`;
