import styled from 'styled-components';

export const Wrapper = styled.div`
  text-align: left;
`;

export const PlaceHolder = styled.div`
  color: #7f8fa4;
`;

export const StyledInput = styled.input`
  width: 100%;
  height: 40px;
  border: 1px solid #dfe3e9;
  border-radius: 4px;
  color: #a6abb2;
  margin-top: 5px;
  padding: 0 15px;
  font-size: 14px;
  background-color: #f8fafc;

  &:focus {
    outline: none;
  }
`;
