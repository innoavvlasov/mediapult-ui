import * as React from 'react';
import Select from '../calendar';

import { Wrapper } from './styled';

export default class Calendar extends React.Component<any, any> {
  day: any;
  month: any;
  year: any;
  constructor(props: any) {
    super(props);
  }
  render() {
    return(
      <Wrapper>
        <Select />
      </Wrapper>
    );
  }
}