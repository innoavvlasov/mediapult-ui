import * as React from 'react';
import { connect } from 'react-redux';
import { Field, FieldArray, formValueSelector, reduxForm } from 'redux-form';

import AllPlan from './allPlan';
import LiftPlan from './liftPlan';
import RadioPlan from './radioPlan';
import TransportPlan from './transportPlan';
import {
  Wrapper,
  LeftPane,
  RightPane,
  BuyMenu,
  TableBuy,
  EmptyTableBuy,
  MenuItem,
  Title,
  CountItem,
  Delimiter,
  CompleteInfo,
  Links
} from './view.styled';

enum TypeMenuItem {
  All = 'ALL',
  Lift = 'LIFT',
  Radio = 'RADIO',
  Transport = 'TRANSPORT'
}

const View = (props: any) => {
  let { change, selectedMenu } = props;
  let countPlan = 3;
  let countLift = 1;
  let countRadio = 1;
  let countTransport = 1;

  return (
    <Wrapper>
      <LeftPane>
        <BuyMenu>
          <MenuItem
            onClick={() => change('selectedMenu', 'ALL')}
            selected={TypeMenuItem.All === selectedMenu}
          >
            <Title>Все планы</Title>
            <CountItem>{countPlan}</CountItem>
          </MenuItem>
          {countLift > 0 && (
            <div>
              <Delimiter />
              <MenuItem
                onClick={() => change('selectedMenu', 'LIFT')}
                selected={TypeMenuItem.Lift === selectedMenu}
              >
                <Title>Лифты</Title>
                <CountItem>{countLift}</CountItem>
              </MenuItem>
            </div>
          )}
          {countRadio > 0 && (
            <div>
              <Delimiter />
              <MenuItem
                onClick={() => change('selectedMenu', 'RADIO')}
                selected={TypeMenuItem.Radio === selectedMenu}
              >
                <Title>Радио</Title>
                <CountItem>{countRadio}</CountItem>
              </MenuItem>
            </div>
          )}
          {countTransport > 0 && (
            <div>
              <Delimiter />
              <MenuItem
                onClick={() => change('selectedMenu', 'TRANSPORT')}
                selected={TypeMenuItem.Transport === selectedMenu}
              >
                <Title>Транспорт</Title>
                <CountItem>{countTransport}</CountItem>
              </MenuItem>
            </div>
          )}
        </BuyMenu>
        {(countPlan === 0 && (
          <EmptyTableBuy>Ваша корзина покупок пуста.</EmptyTableBuy>
        )) || <TableBuy />}
        {TypeMenuItem.All === selectedMenu && <AllPlan />}
        {TypeMenuItem.Lift === selectedMenu && <LiftPlan />}
        {TypeMenuItem.Radio === selectedMenu && <RadioPlan />}
        {TypeMenuItem.Transport === selectedMenu && <TransportPlan />}
      </LeftPane>
      <RightPane>
        <CompleteInfo/>
        <Links/>
      </RightPane>
    </Wrapper>
  );
};

export default View;
