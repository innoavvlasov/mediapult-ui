import * as React from 'react';
import {
  Wrapper,
  Header,
  TableRow,
  Product,
  Summa,
  Remove
} from './styled';
import Selecter from '../../../select';
// import Calendar from './calendar';

class View extends React.Component<any, any> {
  constructor(props: any) {
    super(props);
  }
  product: any = [
    {
      type: 'rearPart',
      name: 'Задняя часть (6 м2)',
      address: 'Поликлиника-Больница 35',
      summa: '2350'
    },
    {
      type: 'around',
      name: 'Вокруг (46,8 м2)',
      address: 'Поликлиника-Больница 35',
      summa: '4750'
    },
    {
      type: 'standart',
      name: 'Стандарт (27 м2)',
      address: 'Поликлиника-Больница 35',
      summa: '17450'
    },
    {
      type: 'rearWindow',
      name: 'Заднее стекло (1,8 м2)',
      address: 'Поликлиника-Больница 35',
      summa: '117450'
    },
    {
      type: 'mobileBill',
      name: 'Мобильная афиша (10 м2)',
      address: 'Поликлиника-Больница 35',
      summa: '47450'
    },
    {
      type: 'strips',
      name: 'Полоски (5 м2)',
      address: 'Поликлиника-Больница 35',
      summa: '450'
    },
    {
      type: 'flag',
      name: 'Флажок (7,3 м2)',
      address: 'Поликлиника-Больница 35',
      summa: '50'
    }
  ];
  countTransport: any = { start: 1, end: 3 };
  countMonth: any = { start: 1, end: 3 };

  render() {
    return (
      <Wrapper>
        <Header>
          <div>Товар</div>
          <div>Количество месяцев</div>
          <div>Количество ТС</div>
          <div>Сумма</div>
          <div>Дата запуска</div>
          <div />
        </Header>
        {this.product.map((val: any, index: any) => {
          return (
            <TableRow key={index}>
              <Product data={val} />
              <Selecter
                range={this.countMonth}
                onChange={(i, value) => console.info('index', i, 'val', value)}
              />
              <Selecter
                range={this.countTransport}
                onChange={(i, value) => console.info('index', i, 'val', value)}
              />
              <Summa>{`${val.summa} ₽`} </Summa>
              <div />
              <Remove onClick={() => console.info('remove', index)} />
            </TableRow>
          );
        })}
      </Wrapper>
    );
  }
}
export default View;
