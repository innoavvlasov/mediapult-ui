import styled from 'styled-components';
import { Colors } from '../../constants';

export const Wrapper = styled.div`
  margin: 10px 0;

  > label {
    display: inline-block;
    cursor: pointer;
  }
`;

export const SvgStyled = styled.svg`
  font-family: 'ProximaNovaSemibold';
  /* font-size: 10px; */
  position: relative;
  transform: scale(1.7);
  padding-left: 10px;
  top: -3px;
`;

export const StyledLabel = styled.label`
  opacity: ${(props: any) => 1};
`;

export const Check = styled.input`
  display: inline;
  vertical-align: top;
  opacity: 0;
  width: 20px;
  height: 20px;
  position: absolute;

  &:not(checked) + label {
    position: relative; /* будем позиционировать псевдочекбокс относительно label */
    padding: 0 0 0 60px; /* оставляем слева от label место под псевдочекбокс */
  }

  &:not(checked) + label::before {
    content: '';
    position: absolute;
    top: 2px;
    left: 0;
    width: 50px;
    height: 16px;
    border-radius: 8px;
    background: ${Colors.notActiveColor};
  }

  &:not(checked) + label::after {
    content: '';
    position: absolute;
    top: -5px;
    left: 0;
    width: 22px;
    height: 22px;
    border-radius: 16px;
    border: 3px solid ${Colors.notActiveColor};
    background: #fff;
    transition: left 0.2s; /* анимация, чтобы чекбокс переключался плавно */
  }

  &:checked + label::before {
    background: ${Colors.mainColor};
  }

  &:checked + label::after {
    border: 3px solid ${Colors.mainColor};
    left: 28px;
  }
`;