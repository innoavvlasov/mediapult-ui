import styled from 'styled-components';

export const StyledDiv = styled.div`
    position: absolute;
    width: 100%;
    height: 20%;
`;

export const StyledImg = styled.img`
    width: 100%;
`;