import * as React from 'react';
import styled from 'styled-components';

import { Check, Wrapper, SvgStyled, StyledLabel } from './styled';
import { CheckboxProps, IconProps } from '../../types';
import { Colors } from '../../constants';

export enum checkBoxType {text, paperIcon}

const makeIcon = (props: IconProps) => {
  let fillColor = Colors.orangeColor;
  let result: string | JSX.Element = props.name;

  if (props.checkType === checkBoxType.paperIcon) {
    result = props.name.length > 2 ? props.name :
    (
      <SvgStyled viewBox="0 0 97 116" height="22">
          <path fill="#051F26"  stroke="#2CC8ED" stroke-width={3} d="M19.7,11.5h41.5c3.4,0,6.6,1.4,8.9,3.8l11.5,12.2c1.2,1.3,1.9,3,1.9,4.8v62.5c0,4.2-3.4,7.7-7.7,7.7H19.6 c-4.5,0-8.1-3.6-8.1-8.1V19.7C11.5,15.2,15.2,11.5,19.7,11.5z"/>
          <text font-size="42" fill="#2CC8ED" transform="matrix(1 0 0 1 20 75.75)">{props.name}</text>
      </SvgStyled>
    );
  }
  return result;
};

export const Checkbox = (props: CheckboxProps) => {
  let isChecked: boolean = props.checked;
  const change = () => {
    isChecked = !isChecked;
    props.onChange({label: props.label, checked: isChecked});
  };

  return (
    <Wrapper>
      <Check
        onClick={change}
        id={props.label}
        type="checkbox"
        className="checkbox"
        checked={isChecked}
      />
      <StyledLabel htmlFor={props.label}>{makeIcon({name: props.label, checkType: props.checkType})}</StyledLabel>
    </Wrapper>
  );
};