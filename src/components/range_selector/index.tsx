import * as React from 'react';
import styled from 'styled-components';

import { Colors } from '../../constants';
import { RcSlider, Wrapper } from './styled';

interface RangeSelectorProps {
  valuesRange: Array<number>;
  value: Array<number>;
  onChange(value: Array<number>): void;
  disabled?: boolean;
}

interface RangeSelectorState {
  currentValue: Array<number>;
  bufferInputsValues: Array<number>;
}
export class RangeSelector extends React.PureComponent <RangeSelectorProps, RangeSelectorState> {

  private maxInput: HTMLInputElement;
  private minInput: HTMLInputElement;
  
  constructor(props: RangeSelectorProps) {
    super(props);

    this.state = {
      currentValue: props.value,
      bufferInputsValues: props.value
    };
  }

  setInputRef = (input: HTMLInputElement): void => {
    this[input.name] = input;
  }

  drop = (dropValues: Array<number>): void => {
    this.setState({currentValue: dropValues});
    this.props.onChange(dropValues);
  }
  
  drag = (dragValues: Array<number>): void => {
    this.setState({
      currentValue: dragValues,
      bufferInputsValues: dragValues
    });
  }

  getValidValues(checkingValues: Array<number>, inputName: string): Array<number> {
    let min: number = checkingValues[0];
    let max: number = checkingValues[1];

    if (min > max) {
      if (inputName === 'minInput') {
        min = max;
      } else {
        max = min;
      }
    }
    min = Math.max(min, this.props.valuesRange[0]);
    max = Math.min(max, this.props.valuesRange[1]);

    return [min, max];
  }
  
  onInputChange = (e) => {
    const min = this.minInput.value === '' ? 0 : parseInt(this.minInput.value, 10);
    const max = this.maxInput.value === '' ? 0 : parseInt(this.maxInput.value, 10);
    
    this.setState({
      bufferInputsValues: [min, max],
      currentValue: this.getValidValues([min, max], e.target.name)
    });
  }
  
  insertInputValues = (e) => {
    const minMax = this.getValidValues(this.state.bufferInputsValues, e.target.name);
    this.setState({
      currentValue: minMax,
      bufferInputsValues: minMax
    });
  }
  
  componentWillReceiveProps(props: RangeSelectorProps) {
    this.setState({
      currentValue: props.value,
      bufferInputsValues: props.value
    });
  }

  keyPressed = (e) => {
    if (e.key === 'Enter') {
      const minMax = this.getValidValues(this.state.bufferInputsValues, e.target.name);
      this.setState({
        currentValue: minMax,
        bufferInputsValues: minMax
      });
      e.target.blur();
    }
    return false;
  }

  render () {
    return (
      <Wrapper disabled={this.props.disabled}>
        <input
          disabled={this.props.disabled}
          type="number"
          ref={this.setInputRef}
          value={this.state.bufferInputsValues[0]}
          onChange={this.onInputChange}
          onBlur={this.insertInputValues}
          onKeyPress={this.keyPressed}
          name="minInput"
        />
        <input
          disabled={this.props.disabled}
          type="number"
          ref={this.setInputRef}
          value={this.state.bufferInputsValues[1]}
          onChange={this.onInputChange}
          onBlur={this.insertInputValues}
          onKeyPress={this.keyPressed}
          name="maxInput"
        />
        <RcSlider
          disabled={this.props.disabled}
          value={this.state.currentValue}
          valuesRange={this.props.valuesRange}
          min={this.props.valuesRange[0]}
          max={this.props.valuesRange[1]}
          allowCross={false}
          onAfterChange={this.drop}
          onChange={this.drag}
        />
      </Wrapper>
    );
  }
}