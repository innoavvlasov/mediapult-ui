import styled from 'styled-components';

export const Logo = styled.div`
  font-size: 30px;
  transform: matrix(0.84081458359814, 0, 0, 0.84081458359814, 0, 0);
  color: rgb(239, 243, 246);
  font-weight: 600;
  .desktop {
    display: none;
  }
  .desktop span:first-child {
    margin-right: 10px;
  }
  .pult {
    color: #354052;
  }

  .desktop.desc {
    font-size: 12px;
    margin: 0;
  }

  @media (min-width: 768px) {
    .mobile {
      display: none;
    }
    .desktop {
      display: block;
    }
  }
`;