import * as React from 'react';
import styled from 'styled-components';

const StandartCss = styled.svg`
  width: 125px;
  height: 35px;
  .cls-1 {
    fill: #00aeef;
  }
  .cls-2,
  .cls-4 {
    fill: none;
  }
  .cls-2,
  .cls-3,
  .cls-4 {
    stroke: #000;
  }
  .cls-2,
  .cls-3 {
    stroke-miterlimit: 10;
  }
  .cls-3 {
    fill: #fff;
  }
  .cls-4 {
    stroke-linecap: round;
    stroke-linejoin: round;
  }
`;

const StandartIcon = (props: any) => {
  return (
    <StandartCss {...props} viewBox="0 0 206.35 54.62">
     <rect className="cls-1" x="0.5" y="25.75" width="97.19" height="21.26" rx="8" ry="8"/>
      <rect className="cls-2" x="0.5" y="4.49" width="97.19" height="42.52" rx="7.59" ry="7.59"/>
      <path className="cls-2" d="M76.43,10.32H97.69V30.06H76.43a3,3,0,0,1-3-3V13.36A3,3,0,0,1,76.43,10.32Z"/>
      <path className="cls-2" d="M53.69,11.08H65.76a3,3,0,0,1,3,3V47H50.65V14.12A3,3,0,0,1,53.69,11.08Z"/>
      <rect className="cls-2" x="6.38" y="11.14" width="18.22" height="17.67" rx="3.04" ry="3.04"/>
      <rect className="cls-2" x="28.59" y="11.14" width="18.22" height="17.67" rx="3.04" ry="3.04"/>
      <line className="cls-2" x1="0.5" y1="33.2" x2="51.1" y2="33.2"/>
      <circle className="cls-3" cx="23.28" cy="47" r="7.12"/>
      <circle className="cls-3" cx="80.98" cy="47" r="7.12"/>
      <line className="cls-4" x1="85.54" y1="0.5" x2="85.54" y2="4.49"/>
      <line className="cls-4" x1="3.92" y1="54.12" x2="93.13" y2="54.12"/>
      <rect className="cls-1" x="108.66" y="25.75" width="97.19" height="21.26" rx="8" ry="8"/>
      <rect className="cls-2" x="108.66" y="4.49" width="97.19" height="42.52" rx="7.59" ry="7.59"/>
      <path className="cls-2" d="M184.59,10.32h21.26V30.06H184.59a3,3,0,0,1-3-3V13.36A3,3,0,0,1,184.59,10.32Z"/>
      <rect className="cls-2" x="114.55" y="11.14" width="18.22" height="17.67" rx="3.04" ry="3.04"/>
      <rect className="cls-2" x="136.75" y="11.14" width="18.22" height="17.67" rx="3.04" ry="3.04"/>
      <rect className="cls-2" x="158.75" y="11.14" width="18.22" height="17.67" rx="3.04" ry="3.04"/>
      <line className="cls-2" x1="108.66" y1="33.2" x2="205.85" y2="33.2"/>
      <circle className="cls-3" cx="131.44" cy="47.01" r="7.12"/>
      <circle className="cls-3" cx="189.14" cy="47.01" r="7.12"/>
      <line className="cls-4" x1="193.7" y1="0.5" x2="193.7" y2="4.49"/>
      <line className="cls-4" x1="112.08" y1="54.12" x2="201.29" y2="54.12"/>
      <line className="cls-2" x1="59.72" y1="11.14" x2="59.72" y2="47"/>
    </StandartCss>
  );
};

const AroundCss = styled.svg`
  width: 125px;
  height: 35px;
    .cls-1{fill:#00aeef;}.cls-2,.cls-4{fill:none;}.cls-2,.cls-3,.cls-4{stroke:#000;}.cls-2,.cls-3{stroke-miterlimit:10;}.cls-3{fill:#fff;}.cls-4{stroke-linecap:round;stroke-linejoin:round;}
`;
const AroundIcon = (props: any) => {
    return (
      <AroundCss {...props} viewBox="0 0 206.81 55.29">
      <rect className="cls-1" x="0.5" y="25.75" width="97.19" height="21.26" rx="8" ry="8"/>
      <rect className="cls-2" x="0.5" y="4.49" width="97.19" height="42.52" rx="7.59" ry="7.59"/>
      <path className="cls-2" d="M76.43,10.32H97.69V30.06H76.43a3,3,0,0,1-3-3V13.36A3,3,0,0,1,76.43,10.32Z"/>
      <rect className="cls-2" x="6.38" y="11.14" width="18.22" height="16.67" rx="3.04" ry="3.04"/>
      <rect className="cls-2" x="28.59" y="11.14" width="18.22" height="16.67" rx="3.04" ry="3.04"/>
      <line className="cls-2" x1="0.5" y1="32.2" x2="50.68" y2="32.2"/>
      <circle className="cls-3" cx="23.28" cy="47" r="7.12"/>
      <circle className="cls-3" cx="80.98" cy="47" r="7.12"/>
      <line className="cls-4" x1="85.54" y1="0.5" x2="85.54" y2="4.49"/>
      <line className="cls-4" x1="3.92" y1="54.12" x2="93.13" y2="54.12"/>
      <line className="cls-4" x1="140.79" y1="0.94" x2="140.79" y2="4.93"/>
      <line className="cls-4" x1="108.9" y1="54.57" x2="152.93" y2="54.57"/>
      <rect className="cls-3" x="140.22" y="39.89" width="7.77" height="14.68" rx="3.88" ry="3.88"/>
      <rect className="cls-3" x="115.4" y="40.11" width="7.77" height="14.68" rx="3.88" ry="3.88"/>
      <rect className="cls-1" x="108.78" y="30.64" width="44.04" height="16.9" rx="8.45" ry="8.45"/>
      <rect className="cls-2" x="108.9" y="4.93" width="43.92" height="42.3" rx="8.5" ry="8.5"/>
      <rect className="cls-2" x="114.97" y="13.72" width="31.66" height="17.78" rx="5.67" ry="5.67"/>
      <circle className="cls-3" cx="116.74" cy="37.32" r="2.54"/>
      <circle className="cls-3" cx="146.63" cy="36.74" r="2.54"/>
      <rect className="cls-2" x="121.21" y="6.91" width="18.78" height="4.41" rx="2.21" ry="2.21"/>
      <line className="cls-4" x1="162.28" y1="54.38" x2="206.31" y2="54.38"/>
      <rect className="cls-3" x="193.6" y="39.7" width="7.77" height="14.68" rx="3.88" ry="3.88"/>
      <rect className="cls-3" x="168.78" y="39.92" width="7.77" height="14.68" rx="3.88" ry="3.88"/>
      <rect className="cls-1" x="162.16" y="22.44" width="44.04" height="24.91" rx="8.5" ry="8.5"/>
      <rect className="cls-2" x="162.28" y="4.74" width="43.92" height="42.3" rx="8.5" ry="8.5"/>
      <rect className="cls-2" x="168.35" y="8.73" width="31.66" height="13.78" rx="5.67" ry="5.67"/>
      <rect className="cls-3" x="168.35" y="26.82" width="9.5" height="3.5" rx="1.75" ry="1.75"/>
      <rect className="cls-3" x="190.56" y="26.82" width="9.5" height="3.5" rx="1.75" ry="1.75"/>
      <rect className="cls-3" x="162.28" y="40.23" width="44.04" height="7" rx="3.5" ry="3.5"/>
      <path className="cls-2" d="M53.71,10.85H65.79a3,3,0,0,1,3,3V46.77H50.68V13.88A3,3,0,0,1,53.71,10.85Z"/>
      <line className="cls-2" x1="59.75" y1="10.91" x2="59.75" y2="46.77"/>
        </AroundCss>
    );
  };

const RearPartCss = styled.svg`
  width: 125px;
  height: 35px;
  .cls-1,.cls-5{fill:#00aeef;}.cls-2{fill:#fff;}.cls-2,.cls-3,.cls-4,.cls-5{stroke:#000;}.cls-2,.cls-4,.cls-5{stroke-miterlimit:10;}.cls-3,.cls-4{fill:none;}.cls-3{stroke-linecap:round;stroke-linejoin:round;}
`;
const RearPartIcon = (props: any) => {
  return (
    <RearPartCss {...props} viewBox="0 0 54.77 61.87">
      <rect className="cls-1" x="0.36" y="0.1" width="53.77" height="50.84" rx="8.5" ry="8.5"/>
      <rect className="cls-2" x="38.75" y="43.18" width="9.49" height="17.92" rx="3.88" ry="3.88"/>
      <rect className="cls-2" x="8.44" y="43.45" width="9.49" height="17.92" rx="3.88" ry="3.88"/>
      <line className="cls-3" x1="0.5" y1="61.1" x2="54.27" y2="61.1"/>
      <rect className="cls-4" x="0.5" y="0.5" width="53.63" height="51.64" rx="8.5" ry="8.5"/>
      <rect className="cls-5" x="7.92" y="5.37" width="38.65" height="16.82" rx="5.67" ry="5.67"/>
      <rect className="cls-2" x="7.92" y="27.46" width="11.6" height="4.28" rx="1.75" ry="1.75"/>
      <rect className="cls-2" x="35.04" y="27.46" width="11.6" height="4.28" rx="1.75" ry="1.75"/>
      <rect className="cls-2" x="0.5" y="42.61" width="53.77" height="8.54" rx="3.5" ry="3.5"/>
    </RearPartCss>
   );
};

const StripsCss = styled.svg`
  width: 125px;
  height: 35px;
  .cls-1{fill:#00aeef;}.cls-2,.cls-4{fill:none;}.cls-2,.cls-3,.cls-4{stroke:#000;}.cls-2,.cls-3{stroke-miterlimit:10;}.cls-3{fill:#fff;}.cls-4{stroke-linecap:round;stroke-linejoin:round;}
`;
const StripsIcon = (props: any) => {
  return (
    <StripsCss {...props} viewBox="0 0 206.35 54.62">
      <rect className="cls-1" x="112.76" y="8.32" width="67.13" height="11.88" rx="5.94" ry="5.94"/>
      <rect className="cls-1" x="3.01" y="8.07" width="53.13" height="11.88" rx="5.94" ry="5.94"/>
      <rect className="cls-2" x="0.5" y="4.49" width="97.19" height="42.52" rx="7.59" ry="7.59"/>
      <path className="cls-2" d="M76.43,10.32H97.69V30.06H76.43a3,3,0,0,1-3-3V13.36A3,3,0,0,1,76.43,10.32Z"/>
      <path className="cls-2" d="M53.69,11.08H65.76a3,3,0,0,1,3,3V47H50.65V14.12A3,3,0,0,1,53.69,11.08Z"/>
      <rect className="cls-2" x="6.38" y="11.14" width="18.22" height="17.67" rx="3.04" ry="3.04"/>
      <rect className="cls-2" x="28.59" y="11.14" width="18.22" height="17.67" rx="3.04" ry="3.04"/>
      <line className="cls-2" x1="0.5" y1="33.2" x2="51.1" y2="33.2"/>
      <circle className="cls-3" cx="23.28" cy="47" r="7.12"/>
      <circle className="cls-3" cx="80.98" cy="47" r="7.12"/>
      <line className="cls-4" x1="85.54" y1="0.5" x2="85.54" y2="4.49"/>
      <line className="cls-4" x1="3.92" y1="54.12" x2="93.13" y2="54.12"/>
      <rect className="cls-2" x="108.66" y="4.49" width="97.19" height="42.52" rx="7.59" ry="7.59"/>
      <path className="cls-2" d="M184.59,10.32h21.26V30.06H184.59a3,3,0,0,1-3-3V13.36A3,3,0,0,1,184.59,10.32Z"/>
      <rect className="cls-2" x="114.55" y="11.14" width="18.22" height="17.67" rx="3.04" ry="3.04"/>
      <rect className="cls-2" x="136.75" y="11.14" width="18.22" height="17.67" rx="3.04" ry="3.04"/>
      <rect className="cls-2" x="158.75" y="11.14" width="18.22" height="17.67" rx="3.04" ry="3.04"/>
      <line className="cls-2" x1="108.66" y1="33.2" x2="205.85" y2="33.2"/>
      <circle className="cls-3" cx="131.44" cy="47.01" r="7.12"/>
      <circle className="cls-3" cx="189.14" cy="47.01" r="7.12"/>
      <line className="cls-4" x1="193.7" y1="0.5" x2="193.7" y2="4.49"/>
      <line className="cls-4" x1="112.08" y1="54.12" x2="201.29" y2="54.12"/>
      <line className="cls-2" x1="59.72" y1="11.14" x2="59.72" y2="47"/>
    </StripsCss>
   );
};

const FlagCss = styled.svg`
  width: 125px;
  height: 35px;
  .cls-1{fill:#00aeef;}.cls-2,.cls-4{fill:none;}.cls-2,.cls-3,.cls-4{stroke:#000;}.cls-2,.cls-3{stroke-miterlimit:10;}.cls-3{fill:#fff;}.cls-4{stroke-linecap:round;stroke-linejoin:round;}
`;
const FlagIcon = (props: any) => {
  return (
    <FlagCss {...props} viewBox="0 0 206.35 54.62">
      <polygon className="cls-1" points="27.08 18.57 23.3 28.26 10.02 28.81 14.77 20.2 27.08 18.57"/>
      <polygon className="cls-1" points="136.06 18.57 132.29 28.26 119 28.81 123.75 20.2 136.06 18.57"/>
      <rect className="cls-1" x="3.01" y="8.07" width="22.22" height="21" rx="8.5" ry="8.5"/>
      <rect className="cls-1" x="112.18" y="8.07" width="22.22" height="21" rx="8.5" ry="8.5"/>
      <path className="cls-1" d="M178.95,20.2H118.7a6,6,0,0,1-5.94-5.94h0a6,6,0,0,1,5.94-5.94h60.25Z"/>
      <path className="cls-1" d="M49.2,19.95H8.95A6,6,0,0,1,3,14H3A6,6,0,0,1,8.95,8.07H49.2Z"/>
      <rect className="cls-2" x="0.5" y="4.49" width="97.19" height="42.52" rx="7.59" ry="7.59"/>
      <path className="cls-2" d="M76.43,10.32H97.69V30.06H76.43a3,3,0,0,1-3-3V13.36A3,3,0,0,1,76.43,10.32Z"/>
      <path className="cls-2" d="M53.69,11.08H65.76a3,3,0,0,1,3,3V47H50.65V14.12A3,3,0,0,1,53.69,11.08Z"/>
      <rect className="cls-2" x="6.38" y="11.14" width="18.22" height="17.67" rx="3.04" ry="3.04"/>
      <rect className="cls-2" x="28.59" y="11.14" width="18.22" height="17.67" rx="3.04" ry="3.04"/>
      <line className="cls-2" x1="0.5" y1="33.2" x2="51.1" y2="33.2"/>
      <circle className="cls-3" cx="23.28" cy="47" r="7.12"/>
      <circle className="cls-3" cx="80.98" cy="47" r="7.12"/>
      <line className="cls-4" x1="85.54" y1="0.5" x2="85.54" y2="4.49"/>
      <rect className="cls-2" x="108.66" y="4.49" width="97.19" height="42.52" rx="7.59" ry="7.59"/>
      <path className="cls-2" d="M184.59,10.32h21.26V30.06H184.59a3,3,0,0,1-3-3V13.36A3,3,0,0,1,184.59,10.32Z"/>
      <rect className="cls-2" x="114.55" y="11.14" width="18.22" height="17.67" rx="3.04" ry="3.04"/>
      <rect className="cls-2" x="136.75" y="11.14" width="18.22" height="17.67" rx="3.04" ry="3.04"/>
      <rect className="cls-2" x="158.75" y="11.14" width="18.22" height="17.67" rx="3.04" ry="3.04"/>
      <line className="cls-2" x1="108.66" y1="33.2" x2="205.85" y2="33.2"/>
      <circle className="cls-3" cx="131.44" cy="47.01" r="7.12"/>
      <circle className="cls-3" cx="189.14" cy="47.01" r="7.12"/>
      <line className="cls-4" x1="193.7" y1="0.5" x2="193.7" y2="4.49"/>
      <line className="cls-2" x1="59.72" y1="11.14" x2="59.72" y2="47"/>
    </FlagCss>
   );
};

const MobileBillCss = styled.svg`
  width: 125px;
  height: 35px;
  .cls-1{fill:#00aeef;}.cls-2,.cls-4{fill:none;}.cls-2,.cls-3,.cls-4{stroke:#000;}.cls-2,.cls-3{stroke-miterlimit:10;}.cls-3{fill:#fff;}.cls-4{stroke-linecap:round;stroke-linejoin:round;}
`;
const MobileBillIcon = (props: any) => {
  return (
    <MobileBillCss {...props} viewBox="0 0 206.35 54.62">
      <rect className="cls-1" x="0.5" y="25.75" width="97.19" height="21.26" rx="8" ry="8"/>
      <rect className="cls-2" x="0.5" y="4.49" width="97.19" height="42.52" rx="7.59" ry="7.59"/>
      <path className="cls-2" d="M76.43,10.32H97.69V30.06H76.43a3,3,0,0,1-3-3V13.36A3,3,0,0,1,76.43,10.32Z"/>
      <path className="cls-2" d="M53.69,11.08H65.76a3,3,0,0,1,3,3V47H50.65V14.12A3,3,0,0,1,53.69,11.08Z"/>
      <rect className="cls-2" x="6.38" y="11.14" width="18.22" height="17.67" rx="3.04" ry="3.04"/>
      <rect className="cls-2" x="28.59" y="11.14" width="18.22" height="17.67" rx="3.04" ry="3.04"/>
      <line className="cls-2" x1="0.5" y1="33.2" x2="51.1" y2="33.2"/>
      <circle className="cls-3" cx="23.28" cy="47" r="7.12"/>
      <circle className="cls-3" cx="80.98" cy="47" r="7.12"/>
      <line className="cls-4" x1="85.54" y1="0.5" x2="85.54" y2="4.49"/>
      <line className="cls-4" x1="3.92" y1="54.12" x2="93.13" y2="54.12"/>
      <rect className="cls-1" x="108.66" y="25.75" width="97.19" height="21.26" rx="8" ry="8"/>
      <rect className="cls-2" x="108.66" y="4.49" width="97.19" height="42.52" rx="7.59" ry="7.59"/>
      <path className="cls-2" d="M184.59,10.32h21.26V30.06H184.59a3,3,0,0,1-3-3V13.36A3,3,0,0,1,184.59,10.32Z"/>
      <rect className="cls-2" x="114.55" y="11.14" width="18.22" height="17.67" rx="3.04" ry="3.04"/>
      <rect className="cls-2" x="136.75" y="11.14" width="18.22" height="17.67" rx="3.04" ry="3.04"/>
      <rect className="cls-2" x="158.75" y="11.14" width="18.22" height="17.67" rx="3.04" ry="3.04"/>
      <line className="cls-2" x1="108.66" y1="33.2" x2="205.85" y2="33.2"/>
      <circle className="cls-3" cx="131.44" cy="47.01" r="7.12"/>
      <circle className="cls-3" cx="189.14" cy="47.01" r="7.12"/>
      <line className="cls-4" x1="193.7" y1="0.5" x2="193.7" y2="4.49"/>
      <line className="cls-4" x1="112.08" y1="54.12" x2="201.29" y2="54.12"/>
      <line className="cls-2" x1="59.72" y1="11.14" x2="59.72" y2="47"/>
    </MobileBillCss>
   );
};

const RearWindowCss = styled.svg`
  width: 125px;
  height: 35px;
  .cls-1{fill:#fff;}.cls-1,.cls-2,.cls-3,.cls-4{stroke:#000;}.cls-1,.cls-3,.cls-4{stroke-miterlimit:10;}.cls-2,.cls-3{fill:none;}.cls-2{stroke-linecap:round;stroke-linejoin:round;}.cls-4{fill:#00aeef;}
`;
const RearWindowIcon = (props: any) => {
  return (
    <RearWindowCss {...props} viewBox="0 0 54.77 61.87">
      <rect className="cls-1" x="38.75" y="43.18" width="9.49" height="17.92" rx="3.88" ry="3.88"/>
      <rect className="cls-1" x="8.44" y="43.45" width="9.49" height="17.92" rx="3.88" ry="3.88"/>
      <line className="cls-2" x1="0.5" y1="61.1" x2="54.27" y2="61.1"/>
      <rect className="cls-3" x="0.5" y="0.5" width="53.63" height="51.64" rx="8.5" ry="8.5"/>
      <rect className="cls-4" x="7.92" y="5.37" width="38.65" height="16.82" rx="5.67" ry="5.67"/>
      <rect className="cls-1" x="7.92" y="27.46" width="11.6" height="4.28" rx="1.75" ry="1.75"/>
      <rect className="cls-1" x="35.04" y="27.46" width="11.6" height="4.28" rx="1.75" ry="1.75"/>
      <rect className="cls-1" x="0.5" y="42.61" width="53.77" height="8.54" rx="3.5" ry="3.5"/>
    </RearWindowCss>
   );
};

export const TransportType = {
    standart: StandartIcon,
    around: AroundIcon,
    rearPart: RearPartIcon,
    strips: StripsIcon,
    flag: FlagIcon,
    mobileBill: MobileBillIcon,
    rearWindow: RearWindowIcon
};