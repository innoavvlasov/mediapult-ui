import * as React from 'react';
import styled from 'styled-components';
import { Colors } from '../../constants';

interface DivProps {
  active?: boolean;
}

export const StaticElement = styled.div `
  font-family: ProximaNovaSemibold;
  cursor: pointer;
  width: 230px;
  height: 35px;
  border: 1px solid silver;
  padding-left: 5px;
  border-radius: 3px 3px 0 0;
  background: #feffff;
  background: linear-gradient(to bottom, #feffff 0%, #f4f4f4 100%, #565c62 100%);
`;

export const ListOption = styled.div `
  background-color: white;
  cursor: pointer;
  width: 230px;
  height: 35px;
  border: 1px solid silver;
  border-collapse: collapse;
  border-radius: 2.5px;
  padding-left: 15px;

  & :hover {
    background-color: ${Colors.graishListBg};
    color: ${Colors.mainColor};
  }
`;

export const StyledIcon = styled.svg `
  width: 20px;
  height: 20px;
  transform: translate(0, 3px);
  margin-right: 5px;
`;
export const SvgIcon = (props: any) => {
    return (
        <StyledIcon {...props} viewBox="0 0 20 20">
            <path
                fill="none"
                stroke="silver"
                className="adr-btn-icon"
                strokeWidth="1.5"
                d="M 10 20 L 5.5 14 C 0 0 20 0 14.5 14 L 14.5 14 L 10 20 z"
            />
            <circle r="2" cx="10" cy="10" fill="silver"/>
        </StyledIcon>
    );
};
const Right = styled.div `
  float: right;
`;
export const SvgArrowUp = (props: any) => {
    return (
        <Right>
            <StyledIcon {...props} viewBox="0 0 3 5">
                <path d="M 0 4 1.5 2.6 3 4" stroke="silver" strokeWidth="0.2" fill="none"/>
            </StyledIcon>
        </ Right>
    );
};
export const SvgArrowDwn = (props: any) => {
    return (
        <Right>
            <StyledIcon {...props} viewBox="0 0 3 5">
                <path d="M 0 2.6 1.5 4 3 2.6" stroke="silver" strokeWidth="0.2" fill="none"/>
            </StyledIcon>
        </ Right>
    );
};
export const List = styled.div `
  position: absolute;
  z-index: 2;
`;
export const VerticalCentr: any = styled.div `
  margin: ${ (props: any) => props.value
    ? props.value
    : 2}px 0 0;
`;

export const StyledIcon2 = styled.svg `
  width: 20px;
  height: 20px;
  transform: translate(0, -20px);
  margin-right: 5px;
`;
export const SvgGalka = (props: any) => {
    return (
        <Right>
            <StyledIcon2 {...props} viewBox="0 0 4 5">
                <path d="M 0 2 1.5 4 4 0" stroke="silver" strokeWidth="0.2" fill="none"/>
            </StyledIcon2>
        </ Right>
    );
};