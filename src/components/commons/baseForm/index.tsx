import * as React from 'react';
import { rollbackState } from '../../../workflow/events';
import {
  Wrapper,
  Header,
  Content,
  HeaderLeftBlock,
  HeaderMiddleBlock,
  HeaderRightBlock,
  Breadcrumbs,
  Breadcrumb,
  Indent,
  City
} from './styled';

const View = (props: any) => {
  let titleText = (props.city) ? props.city : props.title;
  const MiddleHeadBlock = props.middleHeadBlock;
  const RightHeadBlock = props.rightHeadBlock;
  return (
    <Wrapper>
      <Header>
        <HeaderLeftBlock>
        {props.city && <City>Город:</City>}
        {titleText}
        </HeaderLeftBlock>
        <HeaderMiddleBlock>
        {props.middleHeadBlock && <MiddleHeadBlock/>}
        </HeaderMiddleBlock>
        <HeaderRightBlock>
        {props.rightHeadBlock && <RightHeadBlock/>}
        </HeaderRightBlock>
      </Header>
      <Breadcrumbs>
        {props.history.map((val, index) => {
          let arrow = (index > 0) ? '<div>' : '';
          return (<div key={index}>{(index > 0) && <Indent>››</Indent>}<Breadcrumb onClick={() => props.rollback(val.id)} active={props.stateName === val.state} >{val.title}</Breadcrumb></div>);
        })}
      </Breadcrumbs>
      <Content>
        {props.children}
      </Content>
    </Wrapper>
  );
};
export default View;
