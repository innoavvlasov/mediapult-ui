import * as React from 'react';
import { StyledIcon, StyledIconProps, StyledSpinner } from './styled';

interface IconProps extends StyledIconProps {
  type: IconType;
}

enum IconType {
  Location = 1,
  Plus,
  RightArrow,
  LeftArrow,
  Times,
  Spinner
}

// tslint:disable:max-line-length jsx-alignment
const Location: React.SFC<StyledIconProps> = props => (
  <StyledIcon theme={props.theme} xmlns="http://www.w3.org/2000/svg" viewBox="0 0 16 24">
    <path
      d="M12 8a4 4 0 1 0-4 4 4 4 0 0 0 4-4zm4 0a6.52 6.52 0 0 1-.52 2.8L9.8 22.89A2 2 0 0 1 8 24a2 2 0 0 1-1.78-1.11L.51 10.8A6.67 6.67 0 0 1 0 8a8 8 0 0 1 16 0z"/>
  </StyledIcon>
);

const Plus: React.SFC<StyledIconProps> = props => (
  <StyledIcon theme={props.theme} xmlns="http://www.w3.org/2000/svg" viewBox="0 0 22 22">
    <path
      d="M22 9.5v3a1.5 1.5 0 0 1-1.5 1.5H14v6.5a1.5 1.5 0 0 1-1.5 1.5h-3A1.5 1.5 0 0 1 8 20.5V14H1.5A1.5 1.5 0 0 1 0 12.5v-3A1.5 1.5 0 0 1 1.5 8H8V1.5A1.5 1.5 0 0 1 9.5 0h3A1.5 1.5 0 0 1 14 1.5V8h6.5A1.5 1.5 0 0 1 22 9.5z"/>
  </StyledIcon>
);

const RightArrowPath: React.SFC<{}> = () => (
  <path
    d="M23 12.172a2.011 2.011 0 0 1-.578 1.422L12.25 23.766a2.08 2.08 0 0 1-1.422.578 2.042 2.042 0 0 1-1.406-.578L8.25 22.594a2 2 0 0 1 0-2.844l4.578-4.578h-11a1.872 1.872 0 0 1-1.828-2v-2a1.872 1.872 0 0 1 1.828-2h11L8.25 4.578a1.959 1.959 0 0 1-.04-2.772l.04-.04L9.422.594A1.988 1.988 0 0 1 10.828 0a2.025 2.025 0 0 1 1.422.594l10.172 10.172A1.946 1.946 0 0 1 23 12.172z"/>
);

const RightArrow: React.SFC<StyledIconProps> = props => (
  <StyledIcon theme={props.theme} xmlns="http://www.w3.org/2000/svg" viewBox="0 0 22 22">
    <RightArrowPath/>
  </StyledIcon>
);

const LeftArrow: React.SFC<StyledIconProps> = props => (
  <StyledIcon theme={{...props.theme, rotate: 180}} xmlns="http://www.w3.org/2000/svg" viewBox="0 0 22 22">
    <RightArrowPath/>
  </StyledIcon>
);

const Times: React.SFC<StyledIconProps> = props => (
  <StyledIcon theme={props.theme} xmlns="http://www.w3.org/2000/svg" viewBox="0 0 18.562 18.562">
    <path
      d="M18.562 14.937a1.51 1.51 0 0 1-.438 1.062l-2.125 2.125a1.506 1.506 0 0 1-2.124 0L9.281 13.53l-4.594 4.594a1.506 1.506 0 0 1-2.124 0L.438 15.999a1.506 1.506 0 0 1 0-2.124l4.594-4.594L.438 4.687a1.506 1.506 0 0 1 0-2.124L2.563.438a1.506 1.506 0 0 1 2.124 0l4.594 4.594L13.875.438a1.506 1.506 0 0 1 2.124 0l2.125 2.125a1.506 1.506 0 0 1 0 2.124L13.53 9.281l4.594 4.594a1.516 1.516 0 0 1 .438 1.062z"/>
  </StyledIcon>
);

const Spinner: React.SFC<StyledIconProps> = props => (
  <StyledSpinner theme={props.theme} xmlns="http://www.w3.org/2000/svg" viewBox="0 0 133.333 133.333">
    <path d="M60 20v20h13.333V0H60z"/>
    <path
      fillOpacity=".857142857"
      d="M19.6 19.734L14.667 24.8l14 13.867 14 14L47.6 47.6l5.067-4.933-14-14-14.133-14z"
    />
    <path fillOpacity=".714285714" d="M0 66.667v6.667h40V60H0z"/>
    <path
      fillOpacity=".571428571"
      d="M28.267 95.067l-13.6 13.733 5.067 4.933 5.066 4.934 13.867-14 13.867-14L48 86c-2.533-2.533-4.8-4.666-5.333-4.666-.4 0-6.8 6.133-14.4 13.733z"
    />
    <path fillOpacity=".428571429" d="M60 113.333v20h13.333v-40H60z"/>
    <path
      fillOpacity=".285714286"
      d="M84.933 85.067L80 90.133l14.667 14.534 14.666 14.666 4.934-5.066 5.066-4.934-14.666-14.666L89.867 80l-4.934 5.067z"
    />
    <path fillOpacity=".142857143" d="M93.333 66.667v6.667h40V60h-40z"/>
  </StyledSpinner>
);

// tslint:enable:max-line-length jsx-alignment

const icons = new Map<IconType, React.SFC<StyledIconProps>>();
icons.set(IconType.Location, Location);
icons.set(IconType.Plus, Plus);
icons.set(IconType.RightArrow, RightArrow);
icons.set(IconType.LeftArrow, LeftArrow);
icons.set(IconType.Times, Times);
icons.set(IconType.Spinner, Spinner);

const Icon = (props: IconProps) => {
  const ConcreteIcon = icons.get(props.type) as React.SFC<StyledIconProps>;
  return <ConcreteIcon {...props}/>;
};

export { IconType, Icon };
