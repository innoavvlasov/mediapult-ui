import * as React from 'react';
import {
  Wrapper,
} from './styled';

import Header from './header';
import Footer from './footer';

const View = (props: any) => {
  return (
    <Wrapper>
      <Header {...props}/>
      {props.children}
      <Footer {...props}/>      
    </Wrapper>
  );
};
export default View;