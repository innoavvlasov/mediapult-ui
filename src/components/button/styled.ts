import styled, { css } from 'styled-components';

const StyledButton = styled.button`
  -webkit-tap-highlight-color: rgba(0, 0, 0, 0);
  outline: 0;
  display: inline-flex;
  align-items: center;
  font: normal 600 16px/1 ${props => props.theme.fontsStack};
  vertical-align: middle;
  border-radius: 3px;
  padding: 0 ${props => props.theme.paddingRight} 0 ${props => props.theme.paddingLeft};
  height: 36px;
  cursor: ${props => props.theme.disabled ? 'not-allowed' : 'pointer'};
  ${props => props.theme.borderColor ? css` border: 1px solid ${props.theme.borderColor}; ` : css` border: 0; `};
  color: ${props => props.theme.color};
  /* stylelint-disable-next-line value-list-max-empty-lines, declaration-colon-newline-after */
  ${props => props.theme.verticalGradient ? css`
    background:
      linear-gradient(
        to bottom,
        ${props.theme.verticalGradient.topColor},
        ${props.theme.verticalGradient.bottomColor}
      );
  ` : css` background: ${props.theme.backgroundColor || 'transparent'}; `};
`;

export { StyledButton };
