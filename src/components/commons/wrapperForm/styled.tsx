import styled from 'styled-components';

export const Colors = Object.freeze({
  mainColor: '#2ea2f8',
  lightMain: '#00aeef',
  notActiveColor: '#ced0da',
  grayColor: '#848c98',
  white: 'white',
  orangeColor: '#ff7800',
  graishListBg: '#f1f4f8',
  alternativeGraish: '#eff3f6',
  bluish: '#354052'
});

export const Wrapper = styled.div`
  display: grid;
  grid-template-rows: 100px auto 161px;
`;

export const FooterWrapper = styled.div`
  background-image: -moz-linear-gradient(
    180deg,
    rgb(25, 145, 235) 0%,
    rgb(0, 174, 239) 100%
  );
  background-image: -webkit-linear-gradient(
    180deg,
    rgb(25, 145, 235) 0%,
    rgb(0, 174, 239) 100%
  );
  background-image: -ms-linear-gradient(
    180deg,
    rgb(25, 145, 235) 0%,
    rgb(0, 174, 239) 100%
  );
  display: grid;
  grid-template-rows: 100px auto;
`;

export const Header = styled.div`
  background-image: -moz-linear-gradient(
    180deg,
    rgb(25, 145, 235) 0%,
    rgb(0, 174, 239) 100%
  );
  background-image: -webkit-linear-gradient(
    180deg,
    rgb(25, 145, 235) 0%,
    rgb(0, 174, 239) 100%
  );
  background-image: -ms-linear-gradient(
    180deg,
    rgb(25, 145, 235) 0%,
    rgb(0, 174, 239) 100%
  );
  display: grid;
  grid-template-columns: 210px auto 210px;
  align-items: center;
  color: white;
`;

export const Logo = styled.div`
  padding-left: 30px;
  cursor: pointer;
  img {    
    width: 180px;
  }
`;
export const Menu = styled.div`
  display: grid;
  align-items: center;
  justify-content: center;
  text-align: center;
  &:first-child {
    text-align: right;
  }
  &:last-child {
    text-align: left;
  }
`;
export const TopMenu = styled(Menu)`
  grid-template-columns: auto 180px auto;
`;
export const BottomMenu = styled(Menu)`
  grid-template-columns: auto 80px auto;
`;

export const Phone = styled.div`
  font-size: 24px;
`;

export const BaseButton = styled.button`
  border: none;
  background: none;
  color: white;

  &:active {
    border-bottom: 1px white solid;
  }
  &:hover {
    background-color: rgba(255, 255, 255, 0.2);
  }
`;

export const SocialIcon = styled.div`
  text-align: center;
`;

export const Link = styled.text`
  margin-right: 20px;
  color: ${Colors.white};
  margin-bottom: 20px;
`;

export const LogIn = styled.div`
  > button {
    margin-right: 10px;
  }
  button {
    display: inline-block;
    float: left;
  }
`;
export const AuthButton = styled.button`
  width: 115px;
  border: 1px white solid;
  background: none;
  border-radius: 4px;
  color: white;
  
  &:hover {
    background-color: rgba(255, 255, 255, 0.2);
  }
  &:active {
    background: white;
    color: #1991eb;
  }
`;
