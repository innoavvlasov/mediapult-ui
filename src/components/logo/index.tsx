import * as React from 'react';

import { Logo } from './styled';

export default props => {
  return (
    <Logo>
      <div>
        <div className="desktop">
          <span className="media">MEDIA</span>
          <span className="pult">PULT</span>
        </div>
        <div className="desktop desc">Агрегатор оффлайн рекламы</div>
        <div className="mobile">
          <span className="media">M</span>
          <span className="pult">P</span>
        </div>
      </div>
    </Logo>
  );
};
