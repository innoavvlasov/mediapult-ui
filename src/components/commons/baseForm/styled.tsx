import styled from 'styled-components';

export const Wrapper = styled.div`
  display: grid;
  grid-template-rows: 70px 46px auto;
  background-color: #eff3f6;
`;

export const Header = styled.div`
  display: grid;
  grid-template-columns: 210px auto 210px;
  align-items: center;
  background-color: white;
`;

export const City = styled.div`
  font-size: 18px;
  float: left;
  margin-right: 5px;
`;

export const HeaderLeftBlock = styled.div`
  font-size: 18px;
  padding-left: 30px;
  width: 180px;
  font-family: 'ProximaNovaSemibold';
  float: left;
`;

export const HeaderMiddleBlock = styled.div`
  text-align: center;
`;
export const HeaderRightBlock = styled.div`

`;

export const Breadcrumbs: any = styled.div`
  display: inline-block;
  position: relative;
  padding-left: 30px;
  padding-top: 15px;
  div {
    position: relative;
    float: left;
    font-size: 14px;
  }
`;
export const Indent = styled.div`
  margin: 0 5px;
  color: #848c98;
`;
export const Breadcrumb: any = styled.div`
  cursor: pointer;
  ${(props: any) => {
    if (props.active) {
      return 'color: #222c3c';
    } else {
      return 'color: #848c98';
    }
  }};
`;

export const Content = styled.div` 
  margin: 0 30px;
  margin-bottom: 30px;
  min-height: 600px;
  position: relative;
`;
