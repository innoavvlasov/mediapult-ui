import * as React from 'react';
import { PlaceHolder, StyledInput, Wrapper } from './styled';

export const TextInput = ({placeholder, className}) => (
    <Wrapper className={className}>
      <PlaceHolder>{placeholder}</PlaceHolder>
      <StyledInput type="text"/>
    </Wrapper>    
);