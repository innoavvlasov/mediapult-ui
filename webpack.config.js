const path = require("path");
module.exports = {
    entry: "./src/index.tsx",    
    output: {
        filename: "mediapult-ui.js",
        path: path.resolve(__dirname, "dist"),
        libraryTarget: 'amd',
        publicPath: '/extlib/mediapult-ui/'
    },

    resolve: {
        modules: ['node_modules', 'src'],
        extensions: [".webpack.js", ".web.js", ".ts", ".tsx", ".js", ".css"]
    },
    context: __dirname,

    devtool: "source-map",
    module: {
        loaders: [
            { test: /\.tsx?$/, loader: "awesome-typescript-loader" },
            { test: /\.(jpe?g|gif|png|svg|woff|ttf|eot|wav|mp3)$/, use: "file-loader" }
        ],
    },
    externals:{
        "react": "react",
        "react-dom": "react-dom",
        'redux': 'redux',
        'react-redux': 'react-redux',
        'redux-form': 'redux-form',
        'styled-components': 'styled-components'
    }
};