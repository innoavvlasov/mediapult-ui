import * as React from 'react';

import { months } from './constant';
import { getCurrentDate, getCountDay } from './helpers';
import { Wrapper, StyledSelect } from './styled';

interface Option {
  label: string;
  value: any;
}

interface Option {
  label: string;
  value: any;
}

export default class Calendar extends React.Component<any, any> {
  selectedDay: Option;
  selectedMonth: Option;
  selectedYear: Option;
  currentDate: any = getCurrentDate();
  days: any;
  years: any = { start: this.currentDate.year, end: this.currentDate.year + 1 };
  constructor(props: any) {
    super(props);
    let date = props.date.split('.');
    this.selectedYear = {label: date[2], value: date[2]};
    this.selectedMonth = months.filter(month => month.value === parseInt(date[1], 10))[0];
    this.selectedDay = {label: date[0], value: date[0]};
    this.days = { start: 1, end: getCountDay(this.selectedMonth.value, this.selectedYear.value) };
    this.state = {dateChange: false};
  }

  setYear(value: any) {
    this.selectedYear = value;
    this.setMonth(this.selectedMonth);
    this.props.onChange(`${this.selectedDay.value}.${this.selectedMonth.value}.${this.selectedYear.value}`);
    this.setState({dateChange: true});
  }

  setMonth(value: any) {
    this.selectedMonth = value;
    this.days = { start: 1, end: getCountDay(this.selectedMonth.value, this.selectedYear.value) };
    this.selectedDay = {label: '1', value: '1'};
    this.props.onChange(`${this.selectedDay.value}.${this.selectedMonth.value}.${this.selectedYear.value}`);
    this.setState({dateChange: !this.state.dateChange});
  }
  setDay(selectedDay: any) {    
    this.props.onChange(`${selectedDay.value}.${this.selectedMonth.value}.${this.selectedYear.value}`);
  }

  render() {
    return (
      <Wrapper {...this.props}>
        <div>
          <StyledSelect
            disableDD={true}
            range={this.days}
            selected={this.selectedDay}
            onChange={(value) => this.setDay(value)}
          />
        </div>
        <div>
          <StyledSelect
            disableDD={true}
            options={months}
            selected={this.selectedMonth}
            onChange={(value) => this.setMonth(value)}
          />
        </div>
        <div>
          <StyledSelect
            disableDD={true}
            range={this.years}
            selected={this.selectedYear}
            onChange={(value) => this.setYear(value)}
          />
        </div>
      </Wrapper>
    );
  }
}
