import * as React from 'react';
import { Icon, IconType } from '../icons/button';
import { ButtonStyledProps, ButtonTheme, getTheme } from './theme';
import { StyledButton } from './styled';

interface ButtonProps extends ButtonStyledProps {
  leftIcon?: IconType;
  rightIcon?: IconType;

  onClick(): void;
}

class Button extends React.PureComponent<ButtonProps, {}> {

  public render(): React.ReactNode {
    const theme: ButtonTheme = getTheme(this.props);

    return (
      <StyledButton
        theme={theme}
        onClick={this.props.onClick}
        disabled={this.props.disabled}
      >
        {this.props.leftIcon && <Icon type={this.props.leftIcon} theme={theme}/>}
        {this.props.children}
        {this.props.rightIcon && <Icon type={this.props.rightIcon} theme={theme}/>}
      </StyledButton>
    );
  }

}

export default Button;
