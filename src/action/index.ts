import { INIT_FLOW } from './actionTypes';

export const initFlow = (value: any) => ({
    type: INIT_FLOW,
    payload: value
});