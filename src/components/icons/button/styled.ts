import styled, { css, keyframes } from 'styled-components';

interface StyledIconProps {
  theme: IconTheme;
}

interface IconTheme {
  color: string;
  rotate?: number;
}

const StyledSvg = styled.svg`
  padding: 0 10px;
  fill: ${props => props.theme.color};
`;

const StyledIcon = StyledSvg.extend`
  height: 12px;
  ${props => props.theme.rotate ? css` transform: rotate(180deg); ` : ''};
`;

const animation = keyframes` to { transform: rotate(360deg); } `;

const StyledSpinner = StyledSvg.extend`
  height: 20px;
  animation: ${animation} 1500ms steps(8) infinite;
`;

export { StyledIconProps, StyledIcon, StyledSpinner };
