import { checkBoxType } from '../components/checkbox';

export type District = {
  id: number;
  label: string;
  population: number;
};

export type RangeProps = {
  minValue: number;
  maxValue: number;
  action: (from: number, to: number) => void;
};

export interface IconProps {
  name: string;
  checkType?: checkBoxType;
}

export type CheckboxProps = {
  checked: boolean;
  label: string;
  checkType?: checkBoxType;
  onChange: (val: object) => void;
};