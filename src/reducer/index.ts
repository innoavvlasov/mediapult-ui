import * as actionTypes from '../action/actionTypes';

export const workFlowReducer = (state: any = {}, action: any) => {
    switch (action.type) {
        case actionTypes.INIT_FLOW:
            return action.payload;
        default:
            return state;
    }
};