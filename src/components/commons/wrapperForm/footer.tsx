import * as React from 'react';
import {
  FooterWrapper,
  Header,
  Logo,
  BottomMenu,
  BaseButton,
  SocialIcon,
  Phone,
  Link
} from './styled';
import { SocialSvgs } from './svgs';

const logo = require('./img/logo.png');

const View = (props: any) => {
  return (
    <FooterWrapper>
      <Header>
        <Logo onClick={() => props.openApp('defaultapp.bundle.js')}>
          <img src={logo} />
        </Logo>
        <BottomMenu>
          <BaseButton>Носители</BaseButton>
          <BaseButton>Сервис</BaseButton>
          <BaseButton>Контакты</BaseButton>
        </BottomMenu>
        <Phone>+78008886677</Phone>
      </Header>
      <SocialIcon>
        <Link>{SocialSvgs.Getsvg('vk')}</Link>
        <Link>{SocialSvgs.Getsvg('twitter')}</Link>
        <Link>{SocialSvgs.Getsvg('youtube')}</Link>
        <Link>{SocialSvgs.Getsvg('fb')}</Link>
        <Link>{SocialSvgs.Getsvg('g+')}</Link>
        <Link>{SocialSvgs.Getsvg('inst')}</Link>
        <Link>{SocialSvgs.Getsvg('ln')}</Link>
        <Link>{SocialSvgs.Getsvg('ok')}</Link>
      </SocialIcon>
    </FooterWrapper>
  );
};
export default View;
