import * as React from 'react';
import styled from 'styled-components';
import { Colors, dictionary } from '../../constants';
import { District } from '../../types';

import {
    List, ListOption, SvgGalka,
    StaticElement, SvgArrowDwn,
    SvgArrowUp, SvgIcon, VerticalCentr
} from './styled';

interface DropDownState {
    opend: boolean;
    districst: District[];
}

export class DropDown extends React.Component <any, any> {
    constructor(props: any) {
        super(props);

        // console.log(props);
        this.state = {
            opend: false,
            // districts: props.form.city.districts,
        };
    }
    openClose = () => {
        this.setState({ opend: !this.state.opend });
    }
    listOptionClicked = (districtId: number) => {
        // TODO: на toggle_district написать selector
        this.props.toggle_district(
            this.props.form.city.districts
            .find((dist: District) => dist.id === districtId)
        );
    }
    render() {
        return (
            <div>
                <StaticElement onClick={this.openClose}>
                    <VerticalCentr>
                        {SvgIcon(this.props)}
                        {dictionary.districts}
                        {this.state.opend ? SvgArrowUp(this.props) : SvgArrowDwn(this.props)}
                    </VerticalCentr>
                </StaticElement>
                <List>
                    {
                        this.state.opend && this.props.form.city.districts.map((district: any) =>
                            <ListOption onClick={() => this.listOptionClicked(district.id)} key={district.label}>
                                <VerticalCentr value={6}>
                                    <div>{district.label}</div>
                                    {
                                        (this.props.form.filters.districts
                                        .find((dist: any) => dist.id === district.id)) ? <SvgGalka /> : ''
                                    }
                                </VerticalCentr>
                            </ListOption>
                        )
                    }
                </List>
            </div>
        );
    }
}
