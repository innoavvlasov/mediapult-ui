import * as React from 'react';
import styled from 'styled-components';
import { Range } from 'rc-slider';
import { Colors } from '../../constants';

interface Disablable {
  disabled: boolean;
}

export const Wrapper = styled.div`
  height: 45px;
  margin-top: 3px;

  input {
    cursor: ${(props: Disablable) => props.disabled ? 'default' : 'text'};
    padding: 0 10px;
    height: 31px;
    width: calc(50% - 25px);
    display: inline-block;
    font-size: 16px;
    background-color: ${Colors.white};
    color: ${(props: Disablable) => props.disabled ? Colors.notActiveColor : Colors.grayText};
    border: 1px solid ${Colors.lightGray};
    border-radius: 4px;
    box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
  }

  input:nth-child(1) {
    float: left;
  }

  input:nth-child(2) {
    float: right;
  }
`;

export const RcSlider = styled(Range)`
  position: relative;
  top : 40px;
  height: 14px;
  padding-top: 7px;
  width: 100%;
  touch-action: none;

  * {
    box-sizing: border-box;
    -webkit-tap-highlight-color: rgba(0, 0, 0, 0);
  }

  .rc-slider-rail {
    position: absolute;
    width: 100%;
    background-color: #e9e9e9;
    height: 4px;
    border-radius: 2px;
  }

  .rc-slider-track {
    position: absolute;
    left: 0;
    height: 4px;
    border-radius: 6px;
    background-color: ${(props: Disablable) => props.disabled ? Colors.notActiveColor : Colors.mainColor};
  }

  .rc-slider-handle {
    position: absolute;
    margin-left: -6px;
    margin-top: -5px;
    width: 14px;
    height: 14px;
    border: 2px solid ${(props: Disablable) => props.disabled ? Colors.notActiveColor : Colors.mainColor};
    cursor: ${(props: Disablable) => props.disabled ? 'default' : 'pointer' };
    border-radius: 7px;
    background-color: ${Colors.white};
    touch-action: pan-x;
  }

  .rc-slider-handle:active {
    border-color: ${(props: Disablable) => props.disabled ? Colors.notActiveColor : Colors.lightBlue};
    box-shadow: 0 0 5px ${(props: Disablable) => props.disabled ? Colors.notActiveColor : Colors.lightBlue};
    cursor: pointer;
  }
`;
