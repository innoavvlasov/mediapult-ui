import * as React from 'react';
import styled from 'styled-components';

export const Wrapper: any = styled.div`
  user-select: none;
  position: relative;
  width: auto;
  .bordered {
    border-color: rgb(46, 162, 248);
  }
  .flip{
    transform: rotateX(-180deg);
  }
  > div {
    width: auto;
    position: relative;
  }
  .outer {
    height: 75px;
    overflow-y: auto;
  }
`;

export const Input = styled.button`
  cursor: pointer;
  border: 1px solid rgb(206, 208, 218);
  border-radius: 4px;
  background-image: -moz-linear-gradient( 90deg, rgb(242,244,247) 0%, rgb(254,255,255) 100%);
  background-image: -webkit-linear-gradient( 90deg, rgb(242,244,247) 0%, rgb(254,255,255) 100%);
  background-image: -ms-linear-gradient( 90deg, rgb(242,244,247) 0%, rgb(254,255,255) 100%);
  font-size: 14px;
  width: 100%;
  color: #354052;
  display: inline-block;
  position: relative;
  > div:first-child{
    float: left;
    width: calc(100% - 23px);
    padding: 5px 0;
    text-align: center;
  }
  > div:nth-child(2){
    float: right;
    width: 16px;
    padding-top: 3px;
    padding-right: 7px;
  }
  > div.fit {
    width: 100%;
  }
  &:focus {
    outline: none;
  }
`;

const DropDownSVG = styled.svg`
  width: 8px;
  height: 4px;

`;

export const DropDownIcon = (props: any) => {
    return (
    <DropDownSVG {...props} viewBox="0 0 8 4">
        <path fill-rule="evenodd"  fill="rgb(168, 170, 183)" d="M7.253,0.935 L4.323,3.809 C4.106,4.023 3.754,4.023 3.537,3.809 L0.607,0.935 C0.390,0.721 0.390,0.374 0.607,0.160 C0.824,-0.054 1.176,-0.054 1.393,0.160 L3.930,2.649 L6.467,0.160 C6.684,-0.054 7.035,-0.054 7.253,0.160 C7.469,0.374 7.469,0.721 7.253,0.935 Z"/>
   </DropDownSVG>
    );
};

export const OuterList = styled.ul`
  position: absolute; 
  width: calc(100% - 2px);
  border-width: 1px;
  border-color: rgb(46, 162, 248);
  border-style: solid;
  background-color: rgb(255, 255, 255);
  box-shadow: 0px 1px 4px 0px rgba(0, 0, 0, 0.08);
  border-bottom-right-radius: 4px;
  border-bottom-left-radius: 4px;
  z-index: 1;
  top: -6px;
  &::-webkit-scrollbar {
    width: 2px;
  }
 
  &::-webkit-scrollbar-track {
      -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
  }
  
  &::-webkit-scrollbar-thumb {
    background-color: darkgrey;
    outline: 1px solid slategrey;
  }
`;

export const Option = styled.li`
  list-style-type:none;
  font-family: 'ProximaNovaSemibold';
  cursor: pointer;
  display: block;
  font-size: 14px;
  color: rgb(53, 64, 82);
  border-bottom: 1px solid rgb(223, 227, 233);

  &:last-child {
    border-bottom: none;
  }
  
  .optionSelect {
    padding: 3px 0;
    width: 100%;
    display: inline-block;
  }

  .optionSelect:link {
    text-decoration: none;
    color: rgb(53, 64, 82);
  }

  .optionSelect:visited {
    text-decoration: none;
    color: rgb(53, 64, 82);
  }

  .optionSelect:hover {
    color: #2ea2f8;
    background-color: #f1f4f8;
  }

`;