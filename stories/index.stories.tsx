import * as React from "react";
import backgrounds from "@storybook/addon-backgrounds";
import { storiesOf } from "@storybook/react";
import { action } from "@storybook/addon-actions";
import { linkTo } from "@storybook/addon-links";
import { withState } from "@dump247/storybook-state";

import {
  Logo,
  Checkbox,
  Button,
  ButtonType,
  IconType,
  Tile,
  CheckBox
} from "../src";
// логотип
storiesOf("Logo", module)
  .addDecorator(
    backgrounds([{ name: "blue", value: "#2ea2f8", default: true }])
  )
  .add("Логотип", () => <Logo />);

// чекбокс
storiesOf("Checkbox", module).add(
  "default",
  withState({ checked: false }, store => (
    <Checkbox
      checked={store.state.checked}
      label={"check me"}
      onChange={(val: any) => store.set({ checked: val.checked })}
    />
  ))
);

// кнопки
storiesOf("Button", module)
  .addDecorator(
    backgrounds([
      { name: "gray", value: "gray", default: true },
      { name: "blue", value: "#2ea2f8" },
      { name: "white", value: "#fff" }
    ])
  )
  .add(
    "ButtonType.Ascetic",
    withState({ clicks: 0 }, (store: any) => (
      <Button
        type={ButtonType.Ascetic}
        disabled={false}
        onClick={() => store.set({ clicks: store.state.clicks + 1 })}
      >
        Кнопка
      </Button>
    ))
  )
  .add(
    "ButtonType.Border",
    withState({ clicks: 0 }, (store: any) => (
      <Button
        leftIcon={IconType.Location}
        disabled={false}
        type={ButtonType.Border}
        onClick={() => store.set({ clicks: store.state.clicks + 1 })}
      >
        Кнопка
      </Button>
    ))
  )
  .add(
    "ButtonType.Green",
    withState({ clicks: 0 }, (store: any) => (
      <Button
        leftIcon={IconType.RightArrow}
        disabled={false}
        type={ButtonType.Green}
        onClick={() => store.set({ clicks: store.state.clicks + 1 })}
      >
        Кнопка
      </Button>
    ))
  );

//плитка;
storiesOf("Tile", module)
  .addDecorator(backgrounds([{ name: "Grey", value: "#eff3f6" }]))
  .add("Default tile", () => (
    <Tile width={"200px"} height={"200px"}>
      I'm a Tile with contents
    </Tile>
  ));

//круглый чекбокс
storiesOf("Round checkbox", module)
  // .addDecorator(backgrounds([{ name: "Grey", value: "#eff3f6" }]))
  .add("Default checkbox", () => <CheckBox checked={false} />);
