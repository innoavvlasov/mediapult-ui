import * as React from 'react';
import { connect } from 'react-redux';
import {
  Field,
  FieldArray,
  formValueSelector,
  reduxForm,
  change
} from 'redux-form';
import CartView from './view';
import InitValue from './initial-values';
import Wrapper from '../wrapperForm';
import BaseForm from '../baseForm';

class View extends React.Component<any, any> {
  constructor(props: any) {
      super(props);
  }
  render() {
    let { city, title, stateName } = this.props;
    let breadcrumbs = [
      { stateName: '', flow: 'landing', title: 'Главная' },
      {
        stateName: 'transportLanding',
        flow: 'transportFlow',
        title: 'Реклама на транспорте'
      },
      { stateName: 'cartForm', flow: 'transportFlow', title: 'Корзина' }
    ];
    return (
      <Wrapper {...this.props}>
        <BaseForm
          city={city}
          title={title}
          breadcrumbs={breadcrumbs}
          stateName={stateName}
        >
          <CartView {...this.props} />
        </BaseForm>
      </Wrapper>
    );
  }
}

const form = 'cartForm';

const CartForm = reduxForm({
  form
})(View);

const selector = formValueSelector(form);

export default connect(
  (state: any) => {
    let { title, stateName, city } = state.workflow.workflowData;
    return {
      selectedMenu: selector(state, 'selectedMenu') || 'TRANSPORT',
      title,
      stateName,
      city
    };
  }
)(CartForm);
