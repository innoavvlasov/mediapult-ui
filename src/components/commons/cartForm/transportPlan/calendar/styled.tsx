import * as React from 'react';
import styled from 'styled-components';

export const Wrapper: any = styled.div`
  user-select: none;
  cursor: pointer;
  position: relative;
`;